
#include "stdafx.h"
#include "Animation.h"
#include "Frame.h"
#include "Game.h"
#include "Material.h"

Animation::Animation(string _tag, Material* mat, float _texW, float _texH) :
	tag(_tag), material(mat), textureHeight(_texH), textureWidth(_texW), activeFrame(0),
	count(0), maxTime(1), active(false), firstFrameLoaded (false), speed(1)
{
}

Animation::~Animation()
{
	for (int i = 0; i < frames.size(); i++)
	{
		delete frames[i];
	}
}

void Animation::UpdateAnimation()
{
	while (count >= maxTime)
	{
		activeFrame++;
		if (activeFrame >= frames.size())
			activeFrame = 0;
		count -= maxTime;
	}
	
	if (firstFrameLoaded)
	{
		material->GetTexture()->SetPosition(frames[activeFrame]->X / textureWidth, frames[activeFrame]->Y / textureHeight);
		material->GetTexture()->ScaleTexture(frames[activeFrame]->width / textureWidth, frames[activeFrame]->height / textureHeight);
	}

	if (active)
	{
		count += Game::GetInstance()->GetDeltaTime() * speed;
	}
}

void Animation::AddFrame(float _width, float _height, float _x, float _y)
{
	frames.push_back(new Frame(_width, _height, _x, _y));
	firstFrameLoaded = true;
}

void Animation::AddFrame(Frame * _frame)
{
	frames.push_back(_frame);
	firstFrameLoaded = true;
}

void Animation::Start()
{
	active = true;
}

void Animation::Pause()
{
	active = false;
}

void Animation::Stop()
{
	active = false;
	activeFrame = 0;
	count = 0;
}

string Animation::GetTag()
{
	return tag;

}

void Animation::SetSpeed(float _speed)
{
	speed = _speed;
}

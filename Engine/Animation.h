#ifndef ANIMATION__H
#define ANIMATION__H

#include "stdafx.h"

using namespace std;

class Frame;
class Material;

class Animation
{
private:
	vector<Frame*> frames;

	float textureWidth;
	float textureHeight;
	int activeFrame;
	float count;
	float maxTime;
	float speed;

	bool active;
	bool firstFrameLoaded;
	
	Material* material;

	string tag;

public:
	Animation(string _tag, Material* mat, float _texW, float _texH);
	~Animation();
	void UpdateAnimation();
	void AddFrame(float _width, float _height, float _x, float _y);
	void AddFrame(Frame* _frame);
	void Start();
	void Pause();
	void Stop();
	string GetTag();
	void SetSpeed(float _speed);
};

#endif // !ANIMATION__H



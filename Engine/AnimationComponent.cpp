#include "stdafx.h"
#include "AnimationComponent.h"
#include "Animation.h"

AnimationComponent::AnimationComponent() :
	actAnimation(NULL)
{
}


AnimationComponent::~AnimationComponent()
{
}

void AnimationComponent::UpdateComposite()
{
	actAnimation->UpdateAnimation();
}

void AnimationComponent::AddAnimation(Animation * anim)
{
	animations.push_back(anim);
}

void AnimationComponent::Play(string tag)
{
	for (int i = 0; i < animations.size(); i++)
	{
		if (animations[i]->GetTag() == tag)
			actAnimation = animations[i];
	}
	actAnimation->Start();
}

void AnimationComponent::Pause()
{
	actAnimation->Pause();
}

void AnimationComponent::Stop()
{
	actAnimation->Stop();
}



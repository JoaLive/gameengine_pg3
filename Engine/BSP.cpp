#include "stdafx.h"
#include "BSP.h"
#include "Game.h"
#include "Camera.h"
#include "Component.h"
#include "Composite.h"
#include "MeshRenderer.h"

BSP* BSP::instance = NULL;

BSP::BSP()
{
}


BSP::~BSP()
{
}

BSP * BSP::GetInstance()
{
	if (instance == NULL)
		instance = new BSP;
	return instance;
}

std::vector<Component*> BSP::BSPFilter(std::vector<Component*>* entities)
{
	std::vector<Component*> SameAsCamera;

	SetEntidades(entities);
	std::vector<Component*> entitiesToKeep = newEntities;
	newEntities.clear();

	for (int i = 0; i < planes.size(); i++)
	{
		float camResult = D3DXPlaneDotCoord(&planes[i], Game::GetInstance()->GetMainCam()->GetPosition());

		for (int j = 0; j < entitiesToKeep.size(); j++)
		{
			float minResult = D3DXPlaneDotCoord(&planes[i], entitiesToKeep.at(j)->GetBoundingBox().GetMin());
			float maxResult = D3DXPlaneDotCoord(&planes[i], entitiesToKeep.at(j)->GetBoundingBox().GetMax());

			if ((camResult * minResult) > 0 || (camResult * maxResult) > 0)
				SameAsCamera.push_back(entitiesToKeep[j]);
		}

		entitiesToKeep = SameAsCamera;
		SameAsCamera.clear();
	}
	return entitiesToKeep;
}

void BSP::SetEntidades(std::vector<Component*>* entities)
{
	for (int i = 0; i < entities->size(); i++)
	{
		if (dynamic_cast<MeshRenderer*>(entities->at(i)))
			newEntities.push_back(entities->at(i));

		if (dynamic_cast<Composite*>(entities->at(i)) != nullptr)
		{
			SetEntidades(&dynamic_cast<Composite*>(entities->at(i))->components);
		}
	}
}

void BSP::AddPlane(D3DXPLANE plane)
{
	planes.push_back(plane);
}

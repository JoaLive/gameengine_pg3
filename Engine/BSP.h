#pragma once
#include "stdafx.h"
class Component;
class BSP
{
private:
	BSP();
	std::vector<D3DXPLANE> planes;
	std::vector<Component*> newEntities;
public:
	static BSP* instance;
	~BSP();
	static BSP* GetInstance();
	std::vector<Component*> BSPFilter(std::vector<Component*>* entities);
	void BSP::SetEntidades(std::vector<Component*>* entities);
	void AddPlane(D3DXPLANE plane);
};


#include "stdafx.h"
#include "BoundingBox.h"


BoundingBox::BoundingBox() : xMin(+100000), yMin(+100000), zMin(+100000),
xMax(-100000), yMax(-100000), zMax(-1000000)
{
	Refresh();
}

void BoundingBox::Refresh()
{
	vertexes[0] = D3DXVECTOR3(xMax, yMax, zMax);
	vertexes[1] = D3DXVECTOR3(xMax, yMax, zMin);
	vertexes[2] = D3DXVECTOR3(xMin, yMax, zMax);
	vertexes[3] = D3DXVECTOR3(xMin, yMax, zMin);
	vertexes[4] = D3DXVECTOR3(xMax, yMin, zMax);
	vertexes[5] = D3DXVECTOR3(xMax, yMin, zMin);
	vertexes[6] = D3DXVECTOR3(xMin, yMin, zMax);
	vertexes[7] = D3DXVECTOR3(xMin, yMin, zMin);
}

BoundingBox BoundingBox::Transform(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	D3DXMATRIX matTrans;
	D3DXMATRIX matSca;
	D3DXMATRIX matRotX;
	D3DXMATRIX matRotY;
	D3DXMATRIX matRotZ;
	D3DXMatrixTranslation(&matTrans, pos.x, pos.y, pos.z);
	D3DXMatrixScaling(&matSca, sca.x, sca.y, sca.z);
	D3DXMatrixRotationX(&matRotX, D3DXToRadian(rot.x));
	D3DXMatrixRotationY(&matRotY, D3DXToRadian(rot.y));
	D3DXMatrixRotationZ(&matRotZ, D3DXToRadian(rot.z));
	D3DXMATRIX mat = matRotX * matRotY * matRotZ * matSca * matTrans;

	BoundingBox bb;
	return bb;
}

BoundingBox BoundingBox::Transform(D3DXMATRIX mat, BoundingBox bb)
{
	Refresh();
	BoundingBox aux;
	for (int i = 0; i < 8; i++)
	{
		D3DXVECTOR4 transVertex;
		D3DXVec3Transform(&transVertex, &vertexes[i], &mat);

		aux.xMin = min(transVertex.x, aux.xMin);
		aux.yMin = min(transVertex.y, aux.yMin);
		aux.zMin = min(transVertex.z, aux.zMin);

		aux.xMax = max(transVertex.x, aux.xMax);
		aux.yMax = max(transVertex.y, aux.yMax);
		aux.zMax = max(transVertex.z, aux.zMax);
	}
	aux.Refresh();
	return aux;
}

void BoundingBox::Combine(BoundingBox otherBb)
{
	xMin = min(xMin, otherBb.xMin);
	yMin = min(yMin, otherBb.yMin);
	zMin = min(zMin, otherBb.zMin);

	xMax = max(xMax, otherBb.xMax);
	yMax = max(yMax, otherBb.yMax);
	zMax = max(zMax, otherBb.zMax);

	Refresh();
}

D3DXVECTOR3* BoundingBox::GetMin()
{
	return new D3DXVECTOR3(xMin, yMin, zMin);
}

D3DXVECTOR3* BoundingBox::GetMax()
{
	return new D3DXVECTOR3(xMax, yMax, zMax);
}

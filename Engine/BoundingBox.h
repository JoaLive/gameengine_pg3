#ifndef BOUNDINGBOX__H
#define BOUNDINGBOX__H

#include "stdafx.h"

class BoundingBox
{
	D3DXVECTOR3 vertexes[8];

public:
	BoundingBox();

	float xMin, xMax,
		  yMin, yMax,
		  zMin, zMax;

	void Refresh();
	BoundingBox Transform(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca);
	BoundingBox Transform(D3DXMATRIX mat, BoundingBox bb);
	void Combine(BoundingBox otherBb);
	D3DXVECTOR3* GetMin();
	D3DXVECTOR3* GetMax();
};

#endif // !BOUNDINGBOX__H




#include "stdafx.h"
#include "Camera.h"
#include "Plane.h"
#include "BoundingBox.h"

Camera::Camera() : dirtyFlag(false)
{
	m_position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_look = D3DXVECTOR3(0.0f, 0.0f, 1.0f);

	planes[0] = new D3DXPLANE();
	planes[1] = new D3DXPLANE();
	planes[2] = new D3DXPLANE();
	planes[3] = new D3DXPLANE();
	planes[4] = new D3DXPLANE();
	planes[5] = new D3DXPLANE();
	
	MakeFrustrumPlanes();
}

Camera::~Camera()
{
	//delete[] planes;
}

void Camera::CreateProjectionMatrix(float fov, float aspect, float nearPlane, float farPlane)
{
	D3DXMatrixPerspectiveFovLH(&m_projection, fov, aspect, nearPlane, farPlane);
	Game::GetInstance()->GetDevice()->SetTransform(D3DTS_PROJECTION, &m_projection);

	dirtyFlag = true;
}

void Camera::Move(D3DXVECTOR3 pos)
{
	m_position += m_look * pos.z;
	m_position += m_right * pos.x;
	m_position.y += pos.y;

	dirtyFlag = true;
}

void Camera::Yaw(float radians)
{
	if (radians == 0.0f)
		return;

	D3DXMATRIX rotation;
	D3DXMatrixRotationAxis(&rotation, &m_up, radians);
	D3DXVec3TransformNormal(&m_right, &m_right, &rotation);
	D3DXVec3TransformNormal(&m_look, &m_look, &rotation);

	dirtyFlag = true;
}

void Camera::Pitch(float radians)
{
	if (radians == 0.0f)
		return;

	D3DXMATRIX rotation;
	D3DXMatrixRotationAxis(&rotation, &m_right, radians);
	D3DXVec3TransformNormal(&m_up, &m_up, &rotation);
	D3DXVec3TransformNormal(&m_look, &m_look, &rotation);

	dirtyFlag = true;
}

void Camera::Roll(float radians)
{
	if (radians == 0.0f)

		return;

	D3DXMATRIX rotation;
	D3DXMatrixRotationAxis(&rotation, &m_look, radians);
	D3DXVec3TransformNormal(&m_right, &m_right, &rotation);
	D3DXVec3TransformNormal(&m_up, &m_up, &rotation);

	dirtyFlag = true;
}

void Camera::Update()
{
	m_lookAt = m_position + m_look;

	// Calculate the new view matrix
	D3DXVECTOR3 up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&m_view, &m_position, &m_lookAt, &up);

	// Set the camera axes from the view matrix
	m_right.x = m_view._11;
	m_right.y = m_view._21;
	m_right.z = m_view._31;
	m_up.x = m_view._12;
	m_up.y = m_view._22;
	m_up.z = m_view._32;
	m_look.x = m_view._13;
	m_look.y = m_view._23;
	m_look.z = m_view._33;

	if (dirtyFlag)
	{
		MakeFrustrumPlanes();
		dirtyFlag = false;
	}
}

void Camera::SetPosition(D3DXVECTOR3 * pPosition)
{
	m_position.x = pPosition->x;
	m_position.y = pPosition->y;
	m_position.z = pPosition->z;

	dirtyFlag = true;
}

void Camera::SetLookAt(D3DXVECTOR3 * pLookAt)
{
	m_lookAt.x = pLookAt->x;
	m_lookAt.y = pLookAt->y;
	m_lookAt.z = pLookAt->z;
	D3DXVec3Normalize(&m_look, &(m_lookAt - m_position));

	dirtyFlag = true;
}

//viejo
/*
void Camera::Update(LPDIRECT3DDEVICE9 dev)
{
	UpdateViewMat();

	dev->SetTransform(D3DTS_VIEW, &viewMat);
	dev->SetTransform(D3DTS_PROJECTION, &proyMat);

	if (dirtyFlag)
		RecalculatePlanes();
}

D3DXMATRIX Camera::GetViewMat()
{
	return viewMat;
}

D3DXMATRIX Camera::GetProyMat()
{
	return proyMat;
}

D3DXVECTOR4 Camera::GetPosition()
{
	return position;
}

void Camera::UpdateViewMat()
{
	D3DXVECTOR3 pos;
	D3DXVECTOR3 m_lookAt;
	D3DXVECTOR3 up;
	pos.x = position.x;
	pos.y = position.y;
	pos.z = position.z;
	m_lookAt = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	up = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&viewMat, &pos, &m_lookAt, &up);
	//viewMat = traslateMat * (rotXMat * rotYMat * rotZMat);
}

void Camera::SetMatrixToIden()
{
	D3DXMatrixIdentity(&traslateMat);
	D3DXMatrixIdentity(&rotXMat);
	D3DXMatrixIdentity(&rotYMat);
	D3DXMatrixIdentity(&rotZMat);
	D3DXMatrixIdentity(&viewMat);
	D3DXMatrixIdentity(&proyMat);
}

void Camera::RecalculatePlanes()
{
	D3DMATRIX mp = viewMat * proyMat;

	//NEAR
	planes[0]->a = mp._14 + mp._13;
	planes[0]->b = mp._24 + mp._23;
	planes[0]->c = mp._34 + mp._33;
	planes[0]->d = mp._44 + mp._43;
	D3DXPlaneNormalize(planes[0]->d3dxplane, planes[0]->d3dxplane);

	//FAR
	planes[1]->a = mp._14 - mp._13;
	planes[1]->b = mp._24 - mp._23;
	planes[1]->c = mp._34 - mp._33;
	planes[1]->d = mp._44 - mp._43;
	D3DXPlaneNormalize(planes[1]->d3dxplane, planes[1]->d3dxplane);

	//LEFT
	planes[2]->a = mp._14 - mp._11;
	planes[2]->b = mp._24 - mp._21;
	planes[2]->c = mp._34 - mp._31;
	planes[2]->d = mp._44 - mp._41;
	D3DXPlaneNormalize(planes[2]->d3dxplane, planes[2]->d3dxplane);

	//RIGHT
	planes[3]->a = mp._14 + mp._11;
	planes[3]->b = mp._24 + mp._21;
	planes[3]->c = mp._34 + mp._31;
	planes[3]->d = mp._44 + mp._41;
	D3DXPlaneNormalize(planes[3]->d3dxplane, planes[3]->d3dxplane);

	//TOP
	planes[4]->a = mp._14 - mp._12;
	planes[4]->b = mp._24 - mp._22;
	planes[4]->c = mp._34 - mp._32;
	planes[4]->d = mp._44 - mp._42;
	D3DXPlaneNormalize(planes[4]->d3dxplane, planes[4]->d3dxplane);

	//BOTTOM
	planes[5]->a = mp._14 + mp._12;
	planes[5]->b = mp._24 + mp._22;
	planes[5]->c = mp._34 + mp._32;
	planes[5]->d = mp._44 + mp._42;
	D3DXPlaneNormalize(planes[5]->d3dxplane, planes[5]->d3dxplane);

	//for (int i = 0; i < 6; i++)
	//	planes[i]->Normalize();

	dirtyFlag = false;
}


void Camera::Traslate(D3DXVECTOR4 pos)
{
	position = pos;
	//D3DXMatrixTranslation(&traslateMat, pos.x, pos.y, pos.z);
	dirtyFlag = true;
}

void Camera::Rotate(D3DXVECTOR3 rot)
{
	rotXMat._22 = cos(-rot.x);
	rotXMat._32 = -sin(-rot.x);
	rotXMat._23 = sin(-rot.x);
	rotXMat._33 = cos(-rot.x);

	rotYMat._11 = cos(-rot.y);
	rotYMat._31 = -sin(-rot.y);
	rotYMat._13 = sin(-rot.y);
	rotYMat._33 = cos(-rot.y);

	rotZMat._11 = cos(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._22 = cos(-rot.z);
	dirtyFlag = true;
}

void Camera::SetPerspective(FLOAT FOV, FLOAT minDist, FLOAT maxDist)
{
	D3DXMatrixPerspectiveFovLH(&proyMat, D3DX_PI / 3.0f, 1.3f, 0.1f, 1000.0f);
	//Game::GetInstance()->GetDevice()->SetTransform(D3DTS_PROJECTION, &proyMat);
	dirtyFlag = true;
}
*/

void Camera::MakeFrustrumPlanes()
{
	D3DXMATRIX viewProjection;
	D3DXMatrixMultiply(&viewProjection, &m_view, &m_projection);

	planes[0]->a = viewProjection._14 + viewProjection._13;
	planes[0]->b = viewProjection._24 + viewProjection._23;
	planes[0]->c = viewProjection._34 + viewProjection._33;
	planes[0]->d = viewProjection._44 + viewProjection._43;
	D3DXPlaneNormalize(planes[0], planes[0]);

	//FAR
	planes[1]->a = viewProjection._14 - viewProjection._13;
	planes[1]->b = viewProjection._24 - viewProjection._23;
	planes[1]->c = viewProjection._34 - viewProjection._33;
	planes[1]->d = viewProjection._44 - viewProjection._43;
	D3DXPlaneNormalize(planes[1], planes[1]);

	//LEFT
	planes[2]->a = viewProjection._14 - viewProjection._11;
	planes[2]->b = viewProjection._24 - viewProjection._21;
	planes[2]->c = viewProjection._34 - viewProjection._31;
	planes[2]->d = viewProjection._44 - viewProjection._41;
	D3DXPlaneNormalize(planes[2], planes[2]);

	//RIGHT
	planes[3]->a = viewProjection._14 + viewProjection._11;
	planes[3]->b = viewProjection._24 + viewProjection._21;
	planes[3]->c = viewProjection._34 + viewProjection._31;
	planes[3]->d = viewProjection._44 + viewProjection._41;
	D3DXPlaneNormalize(planes[3], planes[3]);

	//TOP
	planes[4]->a = viewProjection._14 - viewProjection._12;
	planes[4]->b = viewProjection._24 - viewProjection._22;
	planes[4]->c = viewProjection._34 - viewProjection._32;
	planes[4]->d = viewProjection._44 - viewProjection._42;
	D3DXPlaneNormalize(planes[4], planes[4]);

	//BOTTOM
	planes[5]->a = viewProjection._14 + viewProjection._12;
	planes[5]->b = viewProjection._24 + viewProjection._22;
	planes[5]->c = viewProjection._34 + viewProjection._32;
	planes[5]->d = viewProjection._44 + viewProjection._42;
	D3DXPlaneNormalize(planes[5], planes[5]);
}

bool Camera::FrustrumCheck(BoundingBox bb)
{
	D3DXVECTOR3* min = bb.GetMin();
	D3DXVECTOR3* max = bb.GetMax();

	bool minIn = true;
	bool maxIn = true;

	for (int i = 0; i < 6; i++)
	{
		int result = D3DXPlaneDotCoord(planes[i], min);
		if (result < 0)
		{
			minIn = false;
			break;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		if (D3DXPlaneDotCoord(planes[i], max) < 0)
		{
			maxIn = false;
			break;
		}
	}
	
	if (minIn || maxIn)
		return true;

	return false;
}

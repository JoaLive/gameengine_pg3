#ifndef CAMERA__H
#define CAMERA__H

#include "EngineApi.h"
#include "Game.h"
#include "stdafx.h"

class BoundingBox;
class Plane;

class ENGINE_API Camera
{
private:
	//viejo
	/*
	D3DXVECTOR4 position;
	D3DXMATRIX traslateMat;
	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;
	D3DXMATRIX viewMat;
	D3DXMATRIX proyMat;
	void UpdateViewMat();
	void SetMatrixToIden();
	void RecalculatePlanes();
	*/

	D3DXMATRIX  m_view;
	D3DXMATRIX  m_projection;
	D3DXVECTOR3 m_right;
	D3DXVECTOR3 m_up;
	D3DXVECTOR3 m_look;
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_lookAt;
	float       m_yaw;
	float       m_pitch;

	D3DXPLANE* planes[6];
	bool dirtyFlag;
	
public:
	Camera();
	~Camera();
	void CreateProjectionMatrix(float fov, float aspect, float nearPlane, float farPlane);
	void Move(D3DXVECTOR3 pos);

	void Yaw(float radians);
	void Pitch(float radians);
	void Roll(float radians);
	void Update(); 
	
	void SetPosition(D3DXVECTOR3* pPosition);
	void SetLookAt(D3DXVECTOR3* pLookAt);

	D3DXMATRIX* GetViewMatrix() { return &m_view; }
	D3DXMATRIX* GetProjectionMatrix() { return &m_projection; }
	D3DXVECTOR3* GetPosition() { return &m_position; }
	D3DXVECTOR3* GetLookAt() { return &m_lookAt; }

	void MakeFrustrumPlanes();
	bool FrustrumCheck(BoundingBox bb);


	//viejo
	/*
	void Traslate(D3DXVECTOR4 pos);
	void Rotate(D3DXVECTOR3 rot);
	void SetPerspective(FLOAT FOV, FLOAT minDist, FLOAT maxDist);
	void Update(LPDIRECT3DDEVICE9 dev);
	D3DXMATRIX GetViewMat();
	D3DXMATRIX GetProyMat();
	D3DXVECTOR4 GetPosition();
	*/
};

#endif // !CAMERA__H




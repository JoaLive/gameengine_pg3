#include "stdafx.h"
#include "Composite.h"
#include "Game.h"
#include "Camera.h"

Composite::Composite()
{
}

Composite::~Composite()
{
}

void Composite::AddComponent(Component * component)
{
	components.push_back(component);
	component->SetParent(this);

	//UpdateParentBBox();
}

void Composite::Remove(Component * component)
{
	for (std::vector<Component*>::iterator iter = components.begin(); iter != components.end(); ++iter)
	{
		if (*iter == component)
		{
			components.erase(iter);
			break;
		}
	}
}

void Composite::Update()
{
	Component::Update();

	UpdateMyBoundingBox();

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Update(&parentRelativeMat);

	CombineChildrenBondingBox();
}

void Composite::Update(D3DXMATRIX * parentMat)
{
	Component::Update(parentMat);

	UpdateMyBoundingBox();

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Update(&parentRelativeMat);
	
	CombineChildrenBondingBox();
}

void Composite::Render()
{
	if (!Game::GetInstance()->GetMainCam()->FrustrumCheck(bb))
	{
		return;
	}
	//else
		//Game::GetInstance()->addEntidadDibujada();

	RenderComposite(&parentRelativeMat);

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Render(&parentRelativeMat);
}

void Composite::Render(D3DXMATRIX* parent)
{
	if (!Game::GetInstance()->GetMainCam()->FrustrumCheck(bb))
	{
		return;
	}
	//else
		//Game::GetInstance()->addEntidadDibujada();

	RenderComposite(&parentRelativeMat);

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Render(&parentRelativeMat);
}

D3DXMATRIX Composite::GetParentModelMat()
{
	D3DXMATRIX aux;
	if (GetParent())
		aux = GetParent()->GetParentModelMat() * ModelMat;
	else
		aux = ModelMat;

	return aux;
}

D3DXVECTOR3 Composite::GetWorldPosition()
{
	D3DXMATRIX transMat = GetParentModelMat();
	D3DXVECTOR3 wPos = { transMat._41, transMat._42, transMat._43 };

	return wPos;
}

D3DXVECTOR3 Composite::GetWorldRotation()
{
	D3DXMATRIX rotX = GetParentWorldRotationX();
	D3DXMATRIX rotY = GetParentWorldRotationY();
	D3DXMATRIX rotZ = GetParentWorldRotationZ();

	float angX;
	float angY;
	float angZ;

	if (parentRelativeMat._11 == 1.0f)
	{
		angX = atan2f(parentRelativeMat._13, parentRelativeMat._34);
		angY = 0;
		angZ = 0;

	}
	else if (parentRelativeMat._11 == -1.0f)
	{
		angX = atan2f(parentRelativeMat._13, parentRelativeMat._34);
		angY = 0;
		angZ = 0;
	}
	else
	{

		angX = atan2(-parentRelativeMat._31, parentRelativeMat._11);
		angY = asin(parentRelativeMat._21);
		angZ = atan2(-parentRelativeMat._23, parentRelativeMat._22);
	}

	D3DXVECTOR3 wRot = { angX, angY, angZ };
	

	return wRot;
}

BoundingBox Composite::GetCombinedBBox(D3DXMATRIX parentModelMat)
{
	if (components.size() == 0)
	{
		BoundingBox aux;
		aux = aux.Transform(parentModelMat * ModelMat, aux);
		bb = aux;
		return bb;
	}

	BoundingBox aux;

	D3DXMATRIX UpdatedparentModelMat = parentModelMat * ModelMat;
	aux = aux.Transform(UpdatedparentModelMat, bb);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(UpdatedparentModelMat));

	bb = aux;
	return bb;
}

void Composite::UpdateParentBBox()
{
	BoundingBox aux;

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetBoundingBox());
	
	bb = aux;

	if (GetParent() != NULL)
		GetParent()->UpdateParentBBox();
}

void Composite::Move(D3DXVECTOR3 dir)
{
	pos += dir;
}

void Composite::Rotate(D3DXVECTOR3 angles)
{
	rot += angles;
}

void Composite::Scale(D3DXVECTOR3 mag)
{
	scale += mag;
}

void Composite::SetPosition(D3DXVECTOR3 _pos) 
{
	pos = _pos;
}

void Composite::SetRotation(D3DXVECTOR3 angles)
{
	rot = angles;
}

void Composite::SetScale(D3DXVECTOR3 mag)
{
	scale = mag;
}

void Composite::UpdateMyBoundingBox()
{
}

void Composite::CombineChildrenBondingBox()
{
	if (components.size() == 0)
		return;

	BoundingBox aux;

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetBoundingBox());

	bb = aux;
}

void Composite::SeekImportantBB()
{
	if (components.size() == 0)
		UpdateParentBBox();
	else
	{
		for (int i = 0; i < components.size(); i++)
		{
			components[i]->SeekImportantBB();
		}
	}
}

D3DXMATRIX Composite::GetParentWorldPosition()
{
	D3DXMATRIX transMat = translateMat;

	if (GetParent() != NULL)
		transMat = *GetParent()->GetParentWorldPosition() * ModelMat;

	return transMat;
}

D3DXMATRIX Composite::GetParentWorldRotationX()
{
	D3DXMATRIX rotX = rotXMat;

	if (GetParent() != NULL)
		rotX = GetParent()->GetParentWorldRotationX() * rotX;

	return rotX;
}

D3DXMATRIX Composite::GetParentWorldRotationY()
{
	D3DXMATRIX rotY = rotYMat;

	if (GetParent() != NULL)
		rotY = GetParent()->GetParentWorldRotationY() * rotY;

	return rotY;
}

D3DXMATRIX Composite::GetParentWorldRotationZ()
{
	D3DXMATRIX rotZ = rotZMat;

	if (GetParent() != NULL)
		rotZ = GetParent()->GetParentWorldRotationZ() * rotZ;

	return rotZ;
}

void Composite::UpdateComposite()
{
}

void Composite::RenderComposite(D3DXMATRIX* model)
{
}


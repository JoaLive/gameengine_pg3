#include "stdafx.h"
#include "EngImporter.h"
#include "Game.h"
#include "ModelList.h"
#include "Material.h"
#include "BSP.h"

EngImporter::EngImporter()
{
}

Composite* EngImporter::Import(const std::string & pFile, Composite* entityToAssing)
{
	Assimp::Importer importer;

	const aiScene* aiScene = importer.ReadFile(pFile,
		aiProcess_Triangulate);	

	scene = aiScene;

	CreateRootEntity(scene->mRootNode);

	return rootEntity;
}

void EngImporter::CreateRootEntity(aiNode* root)
{

	if (root->mNumMeshes > 0)
	{
		std::vector<D3DXVECTOR3> positions;
		std::vector<D3DXVECTOR3> normals;
		std::vector<D3DXVECTOR2> uvs;

		std::vector<WORD> indices;

		//Extraccion de Modelo
		aiMesh* mesh = scene->mMeshes[root->mMeshes[0]];	

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			D3DXVECTOR3 vector;
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;

			positions.push_back(vector);

			D3DXVECTOR3 vector2;
			vector2.x = mesh->mNormals[i].x;
			vector2.y = mesh->mNormals[i].y;
			vector2.z = mesh->mNormals[i].z;

			normals.push_back(vector2);

			D3DXVECTOR2 vector3;
			vector3.x = mesh->mTextureCoords[0][i].x;
			vector3.y = mesh->mTextureCoords[0][i].y;

			uvs.push_back(vector3);
		}

		for (int i = 0; i < mesh->mNumFaces; i++)
		{
			for (int j = 0; j < mesh->mFaces->mNumIndices; j++)
			{
				indices.push_back(mesh->mFaces[i].mIndices[j]);
			}
		}

		Model* model = Game::GetInstance()->modelList->CreateModel(positions, normals, uvs, indices);

		//Extraccion de Material
		aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];
		wchar_t texPath;
		mat->Get(AI_MATKEY_TEXTURE(aiTextureType_NORMALS, 0), texPath);
		LPDIRECT3DTEXTURE9 tex = Game::GetInstance()->CreateTexture(&texPath);
		//LPDIRECT3DTEXTURE9 tex = Game::GetInstance()->CreateTexture(L"blue.jpg");

		Material* newMat = new Material(tex);
		newMat->SetEffect("shaderTexTinte.fx");
		newMat->SetAmbientColor({ 0.1f, 0.1f, 0.1f, 0 });

		D3DXMATRIX transform = MatrixToD3DXMatrix(&root->mTransformation);

		MeshRenderer* entity;
		entity = new MeshRenderer(newMat);
		entity->SetModel(model);

		//ADD TRAMSFORM
		D3DXVECTOR3 pos;
		D3DXVECTOR3 scale;
		D3DXQUATERNION rotQ;
		D3DXMatrixDecompose(&scale, &rotQ, &pos, &transform);
		D3DXVECTOR3 rot(rotQ.x, rotQ.y, rotQ.z);

		entity->SetPosition(pos);
		entity->SetScale(scale);
		entity->SetRotation(rot);

		rootEntity = entity;

		for (int i = 0; i < root->mNumChildren; i++)
		{
			CreateChildrenEntity(root->mChildren[i], rootEntity, &transform);
		}
	}
	
	else
	{
		D3DXMATRIX transform = MatrixToD3DXMatrix(&root->mTransformation);

		Composite* entity;
		entity = new Composite();

		//ADD TRANSFORM
		D3DXVECTOR3 pos;
		D3DXVECTOR3 scale;
		D3DXQUATERNION rotQ;
		D3DXMatrixDecompose(&scale, &rotQ, &pos, &transform);
		D3DXVECTOR3 rot(rotQ.x, rotQ.y, rotQ.z);

		entity->SetPosition(pos);
		entity->SetScale(scale);
		entity->SetRotation(rot);

		rootEntity = entity;

		for (int i = 0; i < root->mNumChildren; i++)
		{
			CreateChildrenEntity(root->mChildren[i], rootEntity, &transform);
		}
	}
}

void EngImporter::CreateChildrenEntity(aiNode* node, Composite* targetParent, D3DXMATRIX* parentTransform)
{
	std::string nodeName(node->mName.C_Str());

	if (node->mNumMeshes > 0 && nodeName.find("BSPPlane") == std::string::npos)
	{
		std::vector<D3DXVECTOR3> positions;
		std::vector<D3DXVECTOR3> normals;
		std::vector<D3DXVECTOR2> uvs;

		std::vector<WORD> indices;

		//Extraccion de Modelo
		aiMesh* mesh = scene->mMeshes[node->mMeshes[0]];

		for (int i = 0; i < mesh->mNumVertices; i++)
		{
			D3DXVECTOR3 vector;
			vector.x = mesh->mVertices[i].x;
			vector.y = mesh->mVertices[i].y;
			vector.z = mesh->mVertices[i].z;

			positions.push_back(vector);

			D3DXVECTOR3 vector2;
			vector2.x = mesh->mNormals[i].x;
			vector2.y = mesh->mNormals[i].y;
			vector2.z = mesh->mNormals[i].z;

			normals.push_back(vector2);

			D3DXVECTOR2 vector3;
			if (mesh->HasTextureCoords(0))
			{
				vector3.x = mesh->mTextureCoords[0][i].x;
				vector3.y = mesh->mTextureCoords[0][i].y;
			}
			else
			{
				vector3.x = 0;
				vector3.y = 0;
			}

			uvs.push_back(vector3);
		}

		for (int i = 0; i < mesh->mNumFaces; i++)
		{
			for (int j = 0; j < mesh->mFaces->mNumIndices; j++)
			{
				indices.push_back(mesh->mFaces[i].mIndices[j]);
			}
		}

		Model* model = Game::GetInstance()->modelList->CreateModel(positions, normals, uvs, indices);

		//Extraccion de Material
		aiMaterial* mat = scene->mMaterials[mesh->mMaterialIndex];

		aiString texPath;
		mat->Get(AI_MATKEY_TEXTURE(aiTextureType_NORMALS, 0), texPath);
		mat->GetTexture(aiTextureType_DIFFUSE, 0, &texPath);

		string string = texPath.C_Str();
		std::wstring widestr = std::wstring(string.begin(), string.end());
		const wchar_t* widecstr = widestr.c_str();

		LPDIRECT3DTEXTURE9 tex = Game::GetInstance()->CreateTexture(widecstr);
		//LPDIRECT3DTEXTURE9 tex = Game::GetInstance()->CreateTexture(L"blue.jpg");

		Material* newMat = new Material(tex);
		newMat->SetEffect("shaderTexTinte.fx");
		newMat->SetAmbientColor({ 0.1f, 0.1f, 0.1f, 0 });

		MeshRenderer* entity;
		entity = new MeshRenderer(newMat);
		entity->SetModel(model);

		targetParent->AddComponent(entity);

		D3DXMATRIX transform = MatrixToD3DXMatrix(&node->mTransformation);

		//ADD TRAMSFORM
		D3DXVECTOR3 pos;
		D3DXVECTOR3 scale;
		D3DXQUATERNION rotQ;
		D3DXMatrixDecompose(&scale, &rotQ, &pos, &transform);
		D3DXVECTOR3 rot(rotQ.x, rotQ.y, rotQ.z);


		entity->SetPosition(pos);
		entity->SetScale(scale);
		entity->SetRotation(rot);

		for (int i = 0; i < node->mNumChildren; i++)
		{
			CreateChildrenEntity(node->mChildren[i], entity, &transform);
		}
	}
	
	else
	{
		if (nodeName.find("BSPPlane") != std::string::npos)
		{
			D3DXPLANE* bspPlane = new D3DXPLANE();

			aiMesh* mesh = scene->mMeshes[node->mMeshes[0]];
			D3DXVECTOR3 vector;
			vector.x = mesh->mVertices[0].x;
			vector.y = mesh->mVertices[0].y;
			vector.z = mesh->mVertices[0].z;
			D3DXVECTOR3 vector2;
			vector2.x = mesh->mVertices[1].x;
			vector2.y = mesh->mVertices[1].y;
			vector2.z = mesh->mVertices[1].z;
			D3DXVECTOR3 vector3;
			vector3.x = mesh->mVertices[2].x;
			vector3.y = mesh->mVertices[2].y;
			vector3.z = mesh->mVertices[2].z;

			D3DXPlaneFromPoints(bspPlane, &vector, &vector2, &vector3);

			BSP::GetInstance()->AddPlane(*bspPlane);
		}

		Composite* entity = new Composite();

		D3DXMATRIX transform = MatrixToD3DXMatrix(&node->mTransformation);

		//ADD TRAMSFORM
		D3DXVECTOR3 pos;
		D3DXVECTOR3 scale;
		D3DXQUATERNION rotQ;
		D3DXMatrixDecompose(&scale, &rotQ, &pos, &(transform * *parentTransform));
		D3DXVECTOR3 rot(rotQ.x, rotQ.y, rotQ.z);

		entity->SetPosition(pos);
		entity->SetScale(scale);
		entity->SetRotation(rot);

		targetParent->AddComponent(entity);

		for (int i = 0; i < node->mNumChildren; i++)
		{
			CreateChildrenEntity(node->mChildren[i], entity, &transform);
		}
	}
}

D3DXMATRIX EngImporter::MatrixToD3DXMatrix(aiMatrix4x4* matrix)
{
	D3DXMATRIX newMat;

	newMat._11 = matrix->a1;
	newMat._12 = matrix->a2;
	newMat._13 = matrix->a3;
	newMat._14 = matrix->a4;

	newMat._21 = matrix->b1;
	newMat._22 = matrix->b2;
	newMat._23 = matrix->b3;
	newMat._24 = matrix->b4;

	newMat._31 = matrix->c1;
	newMat._32 = matrix->c2;
	newMat._33 = matrix->c3;
	newMat._34 = matrix->c4;

	newMat._41 = matrix->d1;
	newMat._42 = matrix->d2;
	newMat._43 = matrix->d3;
	newMat._44 = matrix->d4;

	return newMat;

	return D3DXMATRIX();
}

void EngImporter::DecomposeMatrix(D3DXVECTOR3* pos, D3DXVECTOR3* scale, D3DXVECTOR3* rot, D3DXMATRIX mat)
{
	pos->x = mat._14;
	pos->y = mat._24;
	pos->z = mat._34;

	scale->x = mat._11;
	scale->y = mat._22;
	scale->z = mat._33;

	//rot->x = 90;

	//rot.x = cos()
}



EngImporter::~EngImporter()
{
}

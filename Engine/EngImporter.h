#pragma once
#include "Composite.h"
#include "MeshRenderer.h"

#include <assimp/Importer.hpp>      
#include <assimp/scene.h>           
#include <assimp/postprocess.h>

class EngImporter
{
private:
	const aiScene* scene;
	Composite* rootEntity;
	void CreateRootEntity(aiNode* root);
	void CreateChildrenEntity(aiNode* node, Composite* targetParent, D3DXMATRIX* parentTransform);
	D3DXMATRIX MatrixToD3DXMatrix(aiMatrix4x4* matrix);
	void DecomposeMatrix(D3DXVECTOR3* pos, D3DXVECTOR3* scale, D3DXVECTOR3* rot, D3DXMATRIX mat);
public:
	EngImporter();
	Composite* Import(const std::string& pFile, Composite* entityToAssing);
	~EngImporter();
};


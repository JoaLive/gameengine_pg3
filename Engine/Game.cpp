#include "stdafx.h"
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include <chrono>
#include "Composite.h"
#include "MeshRenderer.h"
#include "TilemapRender.h"
#include "AnimationComponent.h"
#include "Animation.h"
#include "Frame.h"
#include "Material.h"
#include "LightManager.h"
#include "DirectionalLight.h"
#include "SpotLight.h"
#include "PointLight.h"
#include "Texture.h"
#include "ModelList.h"
#include <io.h>
#include <iostream>
#include <fcntl.h>
#include "BSP.h"

#include "EngImporter.h"
#include <assimp\Importer.hpp>

#include <iostream>
#include <stdio.h>
#include <wchar.h>

using namespace std;
using namespace std::chrono;

Game* Game::instance = NULL;

Game::Game(){
	windowHeight = 480;
	windowWidth = 640;
	instance = this;

	modelList = new ModelList();
}

Game::~Game(){}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

D3DXMATRIX GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(rot.z);
	rotZMat._12 = sin(rot.z);
	rotZMat._21 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	return scaMat * rotZMat * transMat;
};

D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	return transMat * rotZMat;
};

int Game::GetMS()
{
	time_point<system_clock> actualTime = system_clock::now();
	system_clock::duration duration = actualTime.time_since_epoch();
	milliseconds ms = duration_cast<milliseconds>(duration);
	return ms.count();
}

float Game::GetDeltaTime()
{
	return deltaTime;
}

Camera * Game::GetMainCam()
{
	return mainCam;
}

void Game::AddEntity(Component * comp)
{
	entities.push_back(comp);
}

void Game::ProcessEntities()
{	
	entidadesDibujadas = 0;

	for (int i = 0; i < entities.size(); i++)
	{
		entities[i]->Update();
	}

	if (Input::GetInstance()->KeyPressed(DIK_T))
		float vIn = entidadesDibujadas;

	std::vector<Component*> newEntities = BSP::GetInstance()->BSPFilter(&entities);
	

	for (int i = 0; i < newEntities.size(); i++)
	{
		newEntities[i]->Render();
	}

	float vIn = entidadesDibujadas;
	char vOutChar[17];
	_gcvt_s(vOutChar, sizeof(vOutChar), vIn, 8);
	wchar_t vOut[17];
	mbstowcs_s(NULL, vOut, sizeof(vOut) / 2, vOutChar, sizeof(vOutChar));

	OutputDebugString(L"Entidades Dibujadas: ");
	OutputDebugString(vOut);
	OutputDebugString(L"\n");
}

float Game::addEntidadDibujada()
{
	entidadesDibujadas++;
	return entidadesDibujadas;
}

void CreateConsoleLog(const LPCWSTR winTitle)
{
	AllocConsole();
	SetConsoleTitle(winTitle);

	int hConHandle;
	long lStdHandle;
	FILE *fp;

	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stdout = *fp;
	setvbuf(stdout, NULL, _IONBF, 0);

	// redirect unbuffered STDIN to the console
	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "r");
	*stdin = *fp;
	setvbuf(stdin, NULL, _IONBF, 0);

	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);
	fp = _fdopen(hConHandle, "w");
	*stderr = *fp;
	setvbuf(stderr, NULL, _IONBF, 0);
}

void Game::Run(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
#pragma region SetUp

	//Creamos la clase de la ventana
	WNDCLASSEX wcex;

	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));


	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

	//Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	HWND hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"Title", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		windowWidth, //Ancho
		windowHeight, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

	//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;


	//Creo la interfaz con la placa de video

	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de vido
		D3DDEVTYPE_HAL, //Soft o hard
		hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&dev); //El device que se crea

#pragma endregion SetUp

	entidadesDibujadas = 0;

	dev->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	dev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);

	Input::GetInstance()->Initialize(hInstance, hWnd);

	//Camara
	Camera cam;
	cam.CreateProjectionMatrix(D3DX_PI / 3.0f, 640 / 480, 0.01f, 1000);
	cam.SetPosition(new D3DXVECTOR3{ 0, 1,-10 });
	mainCam = &cam;

	//CARGA DE MODELOS
	Model modelCube("cube.obj", "model");
	LPDIRECT3DTEXTURE9 redTex = CreateTexture(L"red.jpg");
	Material redMat(redTex);

	//LUCES
	#pragma region Luces
	redMat.SetEffect("shaderTexTinte.fx");
	redMat.SetAmbientColor({ 0.1f, 0.1f, 0.1f,0 });

	DirectionalLight directLight;
	directLight.SetColor({ 1, 1, 1, 0 });
	directLight.SetDirecction({ 0, 0, -1, 0 });
	LightManager::GetInstance()->AddLight(&directLight);

	PointLight pointLight;
	pointLight.SetPosition({ 2, 2, 4 });
	pointLight.SetColor({ 0, 0, 1, 0 });
	pointLight.SetRange(15);
	LightManager::GetInstance()->AddLight(&pointLight);

	PointLight pointLight2;
	pointLight2.SetPosition({ -2, 2, 4 });
	pointLight2.SetColor({ 1, 1, 1, 0 });
	pointLight2.SetRange(15);
	LightManager::GetInstance()->AddLight(&pointLight2);
	#pragma endregion

	//Entitad
	MeshRenderer meshEntity(&redMat);
	meshEntity.SetModel(&modelCube);
	Composite entidad;
	entidad.AddComponent(&meshEntity);
	entidad.Move({ 2, 0, 0 });
	AddEntity(&entidad);

	//Scene load
	EngImporter importer;
	Composite scene = *importer.Import("BSPFinal.obj", &scene);
	AddEntity(&scene);

	int lastFrameMs = GetMS();
	float minFrameTime = 0.0167f;

	float num = 0;

	while (true)
	{
		dev->Clear(0, NULL, D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
		actualMs = GetMS();
		deltaTime = (actualMs - lastFrameMs) / 1000.0f;

		lastFrameMs = actualMs;

		MSG msg;

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}

		Input::GetInstance()->ActualizeKeys();

		dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 0, 0, 0), 1.0f, 0);
		dev->BeginScene();

		num += 0.05f;

		cam.Update();
		dev->SetTransform(D3DTS_VIEW, cam.GetViewMatrix());

		#pragma region Input
		float speed = 0.01f;
		if (Input::GetInstance()->KeyPressed(DIK_W))
			cam.Move({0, 0, 0.1f});
		if (Input::GetInstance()->KeyPressed(DIK_S))
			cam.Move({ 0, 0, -0.1f });
		if (Input::GetInstance()->KeyPressed(DIK_D))
			cam.Move({ 0.1f, 0, 0 });
		if (Input::GetInstance()->KeyPressed(DIK_A))
			cam.Move({ -0.1f, 0, 0 });

		if (Input::GetInstance()->KeyPressed(DIK_I))
			entidad.Move({ 0, 0, 0.1f });
		if (Input::GetInstance()->KeyPressed(DIK_K))
			entidad.Move({ 0, 0, -0.1f });
		if (Input::GetInstance()->KeyPressed(DIK_L))
			entidad.Move({ 0.1f, 0, 0 });
		if (Input::GetInstance()->KeyPressed(DIK_J))
			entidad.Move({ -0.1f, 0, 0 });
		if (Input::GetInstance()->KeyPressed(DIK_U))
			entidad.Move({ 0, 0.1f, 0 });
		if (Input::GetInstance()->KeyPressed(DIK_O))
			entidad.Move({ 0, -0.1f, 0 });

		if (Input::GetInstance()->KeyPressed(DIK_UPARROW))
			cam.Pitch(-0.02f);
		if (Input::GetInstance()->KeyPressed(DIK_DOWNARROW))
			cam.Pitch(0.02f);
		if (Input::GetInstance()->KeyPressed(DIK_LEFTARROW))
			cam.Yaw(-0.02f);
		if (Input::GetInstance()->KeyPressed(DIK_RIGHTARROW))
			cam.Yaw(0.02f);
		#pragma endregion

		ProcessEntities();
		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);
	}
	dev->Release();
	d3d->Release();
}

void Game::CreateBuffers(LPDIRECT3DVERTEXBUFFER9 * vb, int vertexCount, LPDIRECT3DINDEXBUFFER9 * ib, int indexCount)
{
	dev->CreateVertexBuffer(
		vertexCount * sizeof(Vertex),
		D3DUSAGE_WRITEONLY, 
		CUSTOMFVF,				
		D3DPOOL_MANAGED,	
		vb,				
		NULL);				

	dev->CreateIndexBuffer(
		indexCount * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		ib,
		NULL);
}

Game * Game::GetInstance()
{
	if (instance == NULL)
		instance = new Game;
	return instance;
}

LPDIRECT3DDEVICE9 Game::GetDevice()
{
	return dev;
}

LPDIRECT3DTEXTURE9 Game::CreateTexture(const wchar_t*  direction)
{
	LPDIRECT3DTEXTURE9 tex;
	D3DXCreateTextureFromFile(dev, direction, &tex);
	return tex;
}

//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}
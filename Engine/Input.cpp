#include "stdafx.h"
#include "Input.h"

Input* Input::instance = NULL;

Input::Input()
{
}


Input::~Input()
{
}

Input * Input::GetInstance()
{
	if (instance == NULL)
		instance = new Input;
	return instance;
}

void Input::Initialize(HINSTANCE hInstance, HWND hWnd)
{
	LPDIRECTINPUT8 dip;
	DirectInput8Create( hInstance,
						DIRECTINPUT_VERSION,
						IID_IDirectInput8,
						(void**)&dip,
						NULL);

	dip->CreateDevice(GUID_SysKeyboard, &keyDev, NULL);
	keyDev->SetDataFormat(&c_dfDIKeyboard);
	keyDev->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	keyDev->Acquire();
	dip->Release();

	for (size_t i = 0; i < 256; i++)
	{
		currentKeys[i] = 0;
	}
}

void Input::ActualizeKeys()
{
	for (size_t i = 0; i < 256; i++)
	{
		prevKeys[i] = currentKeys[i];
	}
	keyDev->GetDeviceState(sizeof(currentKeys), &currentKeys);
}

void Input::Release()
{
	keyDev->Unacquire();	
	keyDev->Release();
}

bool Input::KeyDown(int key)
{
	if (!prevKeys[key] && currentKeys[key])
		return true;
	return false;
}

bool Input::KeyDown(std::vector<int>* keys)
{
	for (int i = 0; i < keys->max_size(); i++)
	{
		if (!prevKeys[keys->at(i)] && currentKeys[keys->at(i)])
			return true;
	}
	return false;
}

bool Input::KeyUp(int key)
{
	if (prevKeys[key] && !currentKeys[key])
		return true;
	return false;
}

bool Input::KeyUp(std::vector<int>* keys)
{
	for (int i = 0; i < keys->max_size(); i++)
	{
		if (prevKeys[keys->at(i)] && !currentKeys[keys->at(i)])
			return true;
	}
	return false;
}

bool Input::KeyPressed(int key)
{
	if (currentKeys[key])
		return true;
	return false;
}

bool Input::KeyPressed(std::vector<int>* keys)
{
	for (int i = 0; i < keys->max_size(); i++)
	{
		if (currentKeys[keys->at(i)])
			return true;
	}
	return false;
}

void Input::AddKeyMap(std::string code, int key)
{
	if (keyMap[code] == NULL)
		keyMap[code] = new std::vector<int>;
	keyMap[code]->push_back(key);
}

std::vector<int>* Input::GetKeyMap(std::string code)
{
	return keyMap[code];
}
 

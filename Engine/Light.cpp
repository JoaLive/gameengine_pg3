#include "stdafx.h"
#include "Light.h"


Light::Light()
{
}


Light::~Light()
{
}

void Light::SetDirecction(D3DXVECTOR4 value)
{
	direcction = value;
}

void Light::SetColor(D3DXVECTOR4 value)
{
	color = value;
}

void Light::SetRange(float value)
{
	range = value;
}

int Light::GetType()
{
	return lightType;
}

D3DXVECTOR4 Light::GetDirection()
{
	return direcction;
}

D3DXVECTOR4 Light::GetColor()
{
	return color;
}

float Light::GetRange()
{
	return range;
}

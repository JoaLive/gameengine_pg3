#ifndef LIGHT__H
#define	 LIGHT__H
#define DIRECT_LIGHT 0
#define POINT_LIGHT 1
#define SPOT_LIGHT 2
#include "Composite.h"

class Light : public Composite
{
protected:
	int lightType;
	D3DXVECTOR4 direcction;
	D3DXVECTOR4 color;
	float range;

public:
	Light();
	~Light();
	void SetDirecction(D3DXVECTOR4 value);
	void SetColor(D3DXVECTOR4 value);
	void SetRange(float value);
	int GetType();
	D3DXVECTOR4 GetDirection();
	D3DXVECTOR4 GetColor();
	float GetRange();
};


#endif // !LIGHT__H

#include "stdafx.h"
#include "LightManager.h"
#include "Light.h"


LightManager* LightManager::instance = NULL;
LightManager::LightManager()
{
}


LightManager::~LightManager()
{
}

void LightManager::AddLight(Light * light)
{
	lights.insert(std::pair<int, Light*>(lights.size(), light));
}

LightManager * LightManager::GetInstance()
{
	if (instance == NULL)
		instance = new LightManager();
	return instance;
}
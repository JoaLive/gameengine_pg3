#ifndef LIGHTMANAGER__H
#define LIGHTMANAGER_H

#include <map>

class Light;

class LightManager
{
private:
	LightManager();
	static LightManager* instance;
public:
	~LightManager();
	std::map<int, Light*> lights;
	void AddLight(Light* light);
	static LightManager* GetInstance();
};

#endif // !LIGHTMANAGER__H
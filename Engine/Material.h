#ifndef MATERIAL__H
#define MATERIAL__H

#include "Model.h"
#include "Texture.h"
#include <map>

class Material
{
private:
	Texture* tex;
	LPD3DXEFFECT effect;
	bool textureLoaded;
	bool effectActivated;
	D3DXVECTOR4 ambientColor;
	std::map<std::string, int> intMap;
	std::map<std::string, float> floatMap;
	std::map<std::string, D3DXVECTOR4> vectorMap;
	std::map<std::string, D3DXMATRIX> matrixMap;

public:
	Material();
	Material(LPDIRECT3DTEXTURE9 _tex);
	~Material();
	void SetTexture(LPDIRECT3DTEXTURE9 _tex);
	Texture* GetTexture();
	bool IsTextureLoaded();
	bool IsEffectActivated();
	void SetEffect(std::string effPath);
	LPD3DXEFFECT GetEffect();
	void SetAmbientColor(D3DXVECTOR4 col);
	D3DXVECTOR4 GetAmbientColor();
	void SetEffectsValues();
	void SetIntToEffect(std::string name, int value);
	void SetFloatToEffect(std::string name, float value);
	void SetVectorToEffect(std::string name, D3DXVECTOR4 value);
	void SetMatrixToEffect(std::string name, D3DXMATRIX value);
};

#endif
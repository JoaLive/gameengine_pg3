#include "stdafx.h"
#include "MeshRenderer.h"
#include "Material.h"
#include "Camera.h"
#include "CCamera.h"
#include "Game.h"
#include "LightManager.h"
#include "Light.h"
#include "Model.h"

MeshRenderer::MeshRenderer()
{	
}

MeshRenderer::MeshRenderer(Material* _material)
{
	material = _material;
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::UpdateComposite()
{

}

void MeshRenderer::RenderComposite(D3DXMATRIX* modelMat)
{
	LPDIRECT3DDEVICE9 dev = Game::GetInstance()->GetDevice();

	material->GetTexture()->SetTextureSettings(dev);

	dev->SetTexture(0, material->GetTexture()->GetTexture());

	dev->SetTransform(D3DTS_WORLD, modelMat);

	if (material->IsEffectActivated())
	{
		if (LightManager::GetInstance()->lights.size() > 0)
		{
			D3DXMATRIX mvp = *modelMat * *Game::GetInstance()->GetMainCam()->GetViewMatrix() * *Game::GetInstance()->GetMainCam()->GetProjectionMatrix();

			D3DXMATRIX rotX;
			D3DXMATRIX rotY;
			D3DXMATRIX rotZ;
			D3DXVECTOR3 rota = GetWorldRotation();
			D3DXMatrixRotationX(&rotX, D3DXToRadian(rota.x));
			D3DXMatrixRotationY(&rotY, D3DXToRadian(rota.y));
			D3DXMatrixRotationZ(&rotZ, D3DXToRadian(rota.z));
			D3DXMATRIX rotmatrx = rotX * rotY * rotZ;

			bool basePass = true;

			for (int i = 0; i < LightManager::GetInstance()->lights.size(); i++)
			{
				if (LightManager::GetInstance()->lights[i]->GetType() == DIRECT_LIGHT)
				{
					int pass;

					material->GetEffect()->SetMatrix("mvpMat", &mvp);
					material->GetEffect()->SetMatrix("modelMat", modelMat);
					material->GetEffect()->SetMatrix("rotMat", &rotmatrx);
					material->GetEffect()->SetVector("lightDir", &LightManager::GetInstance()->lights[i]->GetDirection());
					material->GetEffect()->SetVector("lightCol", &LightManager::GetInstance()->lights[i]->GetColor());

					D3DXVECTOR4 camPos;
					camPos.x = Game::GetInstance()->GetMainCam()->GetPosition()->x;
					camPos.y = Game::GetInstance()->GetMainCam()->GetPosition()->y;
					camPos.z = Game::GetInstance()->GetMainCam()->GetPosition()->z;
					material->GetEffect()->SetVector("camPos", &camPos);
					
					if (basePass)
					{
						material->GetEffect()->SetVector("ambientCol", &material->GetAmbientColor());
						pass = 0;
					}
					else
						pass = 3;

					material->SetEffectsValues();

					UINT passes;
					material->GetEffect()->Begin(&passes, 0);
					material->GetEffect()->BeginPass(pass);

					dev->SetFVF(CUSTOMFVF);
					dev->SetStreamSource(0, model->GetVertexBuffer(), 0, sizeof(Vertex));
					dev->SetIndices(model->GetIndexBuffer());
					dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->GetVertexCount(), 0, model->GetIndexCount() / 3);

					material->GetEffect()->EndPass();
					basePass = false;
				}

				else if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT || LightManager::GetInstance()->lights[i]->GetType() == POINT_LIGHT)
				{
					int pass;

					D3DXVECTOR4 lightPos = { LightManager::GetInstance()->lights[i]->GetPosition().x, LightManager::GetInstance()->lights[i]->GetPosition().y, LightManager::GetInstance()->lights[i]->GetPosition().z, 0 };

					material->GetEffect()->SetMatrix("mvpMat", &mvp);
					material->GetEffect()->SetMatrix("modelMat", modelMat);
					material->GetEffect()->SetMatrix("rotMat", &rotmatrx);
					material->GetEffect()->SetVector("lightDir", &LightManager::GetInstance()->lights[i]->GetDirection());
					material->GetEffect()->SetVector("lightCol", &LightManager::GetInstance()->lights[i]->GetColor());
					material->GetEffect()->SetVector("lightPos", &lightPos);

					D3DXVECTOR4 camPos;
					camPos.x = Game::GetInstance()->GetMainCam()->GetPosition()->x;
					camPos.y = Game::GetInstance()->GetMainCam()->GetPosition()->y;
					camPos.z = Game::GetInstance()->GetMainCam()->GetPosition()->z;

					material->GetEffect()->SetVector("camPos", &camPos);
					material->GetEffect()->SetFloat("lightRange", LightManager::GetInstance()->lights[i]->GetRange());

					if (basePass) 
					{
						material->GetEffect()->SetVector("ambientCol", &material->GetAmbientColor());
						if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT)
							pass = 1;
						else
							pass = 2;
					}
					else
					{
						if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT)
							pass = 4;
						else
							pass = 5;
					}

					material->SetEffectsValues();

					UINT passes;
					material->GetEffect()->Begin(&passes, 0);

					material->GetEffect()->BeginPass(pass);

					dev->SetFVF(CUSTOMFVF);
					dev->SetStreamSource(0, model->GetVertexBuffer(), 0, sizeof(Vertex));
					dev->SetIndices(model->GetIndexBuffer());
					dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->GetVertexCount(), 0, model->GetIndexCount() / 3);
					material->GetEffect()->EndPass();
					basePass = false;
				}
			}
			material->GetEffect()->End();
		}
		else
		{
			material->SetEffectsValues();

			UINT passes;
			material->GetEffect()->Begin(&passes, 0);		
			for (int i = 0; i < passes; i++)
			{
				material->GetEffect()->BeginPass(i);
				dev->SetFVF(CUSTOMFVF);
				dev->SetStreamSource(0, model->GetVertexBuffer(), 0, sizeof(Vertex));
				dev->SetIndices(model->GetIndexBuffer());
				dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->GetVertexCount(), 0, model->GetIndexCount() / 3);
				material->GetEffect()->EndPass();
			}
			material->GetEffect()->End();
		}
	}
	else
	{
		dev->SetFVF(CUSTOMFVF);
		dev->SetStreamSource(0, model->GetVertexBuffer(), 0, sizeof(Vertex));
		dev->SetIndices(model->GetIndexBuffer());
		dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->GetVertexCount(), 0, model->GetIndexCount() / 3);
		dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, model->GetVertexCount(), 0, model->GetIndexCount() / 3);
	}	
}

void MeshRenderer::SetMaterial(Material * _mat)
{
	material = _mat;
}

void MeshRenderer::SetModel(Model * _mod)
{
	model = _mod;
}

BoundingBox MeshRenderer::GetCombinedBBox(D3DXMATRIX parentModelMat)
{	
	BoundingBox aux;
	D3DXMATRIX UpdatedParentModelMat = parentModelMat * ModelMat;

	aux = model->GetBoundingBox().Transform(UpdatedParentModelMat, aux);

	if (components.size() == 0)
	{
		bb = aux;
		return bb;
	}

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetCombinedBBox(UpdatedParentModelMat));

	bb = aux;
	return bb;
}

void MeshRenderer::UpdateParentBBox()
{
	BoundingBox aux;
	aux = model->GetBoundingBox().Transform(GetParentModelMat(), aux);

	for (int i = 0; i < components.size(); i++)
		aux.Combine(components[i]->GetBoundingBox());

	bb = aux;

	if (GetParent() != NULL)
		GetParent()->UpdateParentBBox();
}

void MeshRenderer::Move(D3DXVECTOR3 dir)
{
	pos += dir;
}

void MeshRenderer::Rotate(D3DXVECTOR3 angles)
{
	rot += angles;
}

void MeshRenderer::Scale(D3DXVECTOR3 mag)
{
	scale += mag;
}

void MeshRenderer::SetRotation(D3DXVECTOR3 angles)
{
	rot = angles;
}

void MeshRenderer::SetScale(D3DXVECTOR3 mag)
{
	scale = mag;
}

void MeshRenderer::UpdateMyBoundingBox()
{
	BoundingBox aux;
	aux = model->GetBoundingBox().Transform(parentRelativeMat, model->GetBoundingBox());
	bb = aux;
}



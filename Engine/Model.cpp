#include "stdafx.h"
#include "Model.h"
#include "Game.h"
#include "BoundingBox.h"

Model::Model(std::vector<D3DXVECTOR3> positions, std::vector<D3DXVECTOR3> normals, std::vector<D3DXVECTOR2> uvs, std::vector<WORD> indices)
{
	std::vector<Vertex> ver;

	for (int i = 0; i < positions.size(); i++)
	{
		Vertex vertex;

		vertex.x = positions[i].x;
		vertex.y = positions[i].y;
		vertex.z = positions[i].z;
		
		vertex.tu = uvs[i].x;
		vertex.tv = uvs[i].y;
		
		vertex.nx = normals[i].x;
		vertex.ny = normals[i].y;
		vertex.nz = normals[i].z;

		ver.push_back(vertex);
	}

	vertexes = ver;
	indexes = indices;

	vertexCount = vertexes.size();
	indexCount = indexes.size();

	for (int i = 0; i < positions.size(); i++)
	{
		bb.xMin = min(positions[i].x, bb.xMin);
		bb.yMin = min(positions[i].y, bb.yMin);
		bb.zMin = min(positions[i].z, bb.zMin);

		bb.xMax = max(positions[i].x, bb.xMax);
		bb.yMax = max(positions[i].y, bb.yMax);
		bb.zMax = max(positions[i].z, bb.zMax);
	}

	LPDIRECT3DDEVICE9 dev = Game::GetInstance()->GetDevice();
	Game::GetInstance()->GetDevice()->CreateVertexBuffer(vertexes.size() * sizeof(Vertex), D3DUSAGE_WRITEONLY, CUSTOMFVF, D3DPOOL_MANAGED, &vb, NULL);
	Game::GetInstance()->GetDevice()->CreateIndexBuffer(indexes.size() * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib, NULL);

	VOID* lockedData = NULL;
	vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	ib->Unlock();
}

Model::Model(std::string path, std::string _tag)
{
	LoadModel(path);
	vertexCount = vertexes.size();
	indexCount = indexes.size();
	tag = _tag;

	LPDIRECT3DDEVICE9 dev = Game::GetInstance()->GetDevice();
	Game::GetInstance()->GetDevice()->CreateVertexBuffer(vertexes.size() * sizeof(Vertex), D3DUSAGE_WRITEONLY, CUSTOMFVF, D3DPOOL_MANAGED, &vb, NULL);
	Game::GetInstance()->GetDevice()->CreateIndexBuffer(indexes.size() * sizeof(WORD), D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib, NULL);

	VOID* lockedData = NULL;
	vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	ib->Unlock();
}

bool Model::LoadModel(std::string path)
{
	std::vector<D3DXVECTOR3> positions;
	std::vector<D3DXVECTOR3> normals;
	std::vector<D3DXVECTOR2> uvs;

	FILE* file;
	fopen_s(&file, path.data(), "r");


	while (!feof(file))
	{
		char lineHeader[128];
		fscanf(file, "%s", lineHeader);

		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 position;
			fscanf(file, "%f %f %f\n", &position.x, &position.y, &position.z);

			bb.xMin = min(position.x, bb.xMin);
			bb.yMin = min(position.y, bb.yMin);
			bb.zMin = min(position.z, bb.zMin);

			bb.xMax = max(position.x, bb.xMax);
			bb.yMax = max(position.y, bb.yMax);
			bb.zMax = max(position.z, bb.zMax);

			positions.push_back(position);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uvs.push_back(uv);
		}

		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			Vertex vertex;
			int posIndex, uvIndex, normalIndex, indice;

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;

			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}


			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;

			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}

			fscanf(file, "%d/%d/%d\n", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;

			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;

			if (IsVertexLoaded(vertex, &indice))
				indexes.push_back(indice);
			else
			{
				indexes.push_back(vertexes.size());
				vertexes.push_back(vertex);
			}
		}
	}

	bb.Refresh();

	return true;
}

bool Model::IsVertexLoaded(Vertex vertex, int* indice)
{
	for (int i = 0; i < vertexes.size(); i++)
	{
		if (vertexes[i].x == vertex.x)
		{
			if (vertexes[i].y == vertex.y)
			{
				if (vertexes[i].z == vertex.z)
				{
					if (vertexes[i].nx == vertex.nx)
					{
						if (vertexes[i].ny == vertex.ny)
						{
							if (vertexes[i].nz == vertex.nz)
							{
								if (vertexes[i].tu == vertex.tu)
								{
									if (vertexes[i].tv == vertex.tv)
									{
										*indice = i;
										return true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return false;
}

Model::~Model()
{
}

void Model::AssingVertexAndIndexes(std::vector<Vertex> ver, std::vector<WORD> ind)
{

}

int Model::GetVertexCount()
{
	return vertexCount;
}

int Model::GetIndexCount()
{
	return indexCount;
}

std::string Model::GetTag()
{
	return tag;
}

LPDIRECT3DVERTEXBUFFER9 Model::GetVertexBuffer()
{
	return vb;
}

LPDIRECT3DINDEXBUFFER9 Model::GetIndexBuffer()
{
	return ib;
}

BoundingBox Model::GetBoundingBox()
{
	return bb;
}

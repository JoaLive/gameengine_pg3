#ifndef MODEL_H
#define MODEL_H

#include "EngineApi.h"
#include "stdafx.h"
#include "BoundingBox.h"

class BoundingBox;

struct Vertex
{
	FLOAT x, y, z;
	FLOAT nx, ny, nz;
	FLOAT tu, tv;
};

class ENGINE_API Model
{
private:
	std::vector<Vertex> vertexes;
	std::vector<WORD> indexes;
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
	int vertexCount;
	int indexCount;
	std::string tag;
	bool LoadModel(std::string path);
	bool IsVertexLoaded(Vertex vertex, int* indice);
	BoundingBox bb;
public:
	Model(std::vector<D3DXVECTOR3> positions, std::vector<D3DXVECTOR3> normals, std::vector<D3DXVECTOR2> uvs, std::vector<WORD> indices);
	Model(std::string path, std::string _tag);
	~Model();
	void AssingVertexAndIndexes(std::vector<Vertex> ver, std::vector<WORD> ind);
	int GetVertexCount();
	int GetIndexCount();
	std::string GetTag();
	LPDIRECT3DVERTEXBUFFER9 GetVertexBuffer();
	LPDIRECT3DINDEXBUFFER9 GetIndexBuffer();
	BoundingBox GetBoundingBox();
};

#endif // !MODEL_H


#include "stdafx.h"
#include "ModelList.h"
#include "Model.h"

ModelList::ModelList()
{
}


ModelList::~ModelList()
{
}

Model* ModelList::CreateModel(std::vector<D3DXVECTOR3> positions, std::vector<D3DXVECTOR3> normals, std::vector<D3DXVECTOR2> uvs, std::vector<WORD> indices)
{
	Model* mesh = new Model(positions, normals, uvs, indices);
	models.push_back(mesh);
	return mesh;
}

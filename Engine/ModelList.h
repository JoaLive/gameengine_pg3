#pragma once
#include<vector>
#include<map>
class Model;

class ModelList
{
private:
	std::vector<Model*> models;
	std::map < std::string, int> tags;
public:
	ModelList();
	~ModelList();
	Model* CreateModel(std::vector<D3DXVECTOR3> positions, std::vector<D3DXVECTOR3> normals, std::vector<D3DXVECTOR2> uvs, std::vector<WORD> indices);
};


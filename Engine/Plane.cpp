#include "stdafx.h"
#include "Plane.h"
#include <cmath>

Plane::Plane(float a, float b, float c, float d) :
	a(a), b(b), c(c), d(d)
{
	d3dxplane = new D3DXPLANE(a, b, c, d);
}

void Plane::Normalize()
{
	float mag = sqrt(a*a + b*b + c*c);
	a /= mag;
	b /= mag;
	c /= mag;
}



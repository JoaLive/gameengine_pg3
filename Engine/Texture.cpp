#include "stdafx.h"
#include "Texture.h"
#include "Game.h"

Texture::Texture(LPDIRECT3DTEXTURE9 _tex)
{
	tex = _tex;
	D3DXMatrixIdentity(&texPosMat);
	D3DXMatrixIdentity(&texScaleMat);
	D3DXMatrixIdentity(&texRotMat);

	wrapU = D3DTADDRESS_CLAMP;
	wrapV = D3DTADDRESS_CLAMP;

	filterMAG = D3DTEXF_NONE;
	filterMIN = D3DTEXF_NONE;
	filterMIP = D3DTEXF_NONE;

	blendEnabled = false;
	additiveBlendEnabled = false;
	multiplyBlendEnabled = false;
	alphaBlendEnabled = false;
}


Texture::~Texture()
{
}

void Texture::SetTextureSettings(LPDIRECT3DDEVICE9 dev)
{

	dev->SetSamplerState(0, D3DSAMP_ADDRESSU, wrapU);
	dev->SetSamplerState(0, D3DSAMP_ADDRESSV, wrapV);
	
	dev->SetSamplerState(0, D3DSAMP_MAGFILTER, filterMAG);
	dev->SetSamplerState(0, D3DSAMP_MINFILTER, filterMIN);
	dev->SetSamplerState(0, D3DSAMP_MIPFILTER, filterMIP);

	if (blendEnabled)
	{
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);

		if (alphaBlendEnabled)
		{
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		}
		else if (additiveBlendEnabled)
		{
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		}
		else if (multiplyBlendEnabled)
		{
			dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_DESTCOLOR);
			dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
		}
	}
	else
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);


	dev->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);

	dev->SetTransform(D3DTS_TEXTURE0, &(texRotMat * texScaleMat * texPosMat));

}

LPDIRECT3DTEXTURE9 Texture::GetTexture()
{
	return tex;
}

void Texture::SetPosition(float _x, float _y)
{
	texPosMat._31 = _x;
	texPosMat._32 = _y;
}

void Texture::SetPosition(D3DXVECTOR2 pos)
{
	texPosMat._31 = pos.x;
	texPosMat._32 = pos.y;
}

void Texture::RotateTexture(float angle)
{
	texRotMat._11 = cos(angle);
	texRotMat._21 = -sin(angle);
	texRotMat._12 = sin(angle);
	texRotMat._22 = cos(angle);
}

void Texture::ScaleTexture(D3DXVECTOR3 sca)
{
	texScaleMat._11 = sca.x;
	texScaleMat._22 = sca.y;
}

void Texture::ScaleTexture(float _x, float _y)
{
	texScaleMat._11 = _x;
	texScaleMat._22 = _y;
}

void Texture::SetWrapModeU(DWORD value)
{
	wrapU = value;
}

void Texture::SetWrapModeV(DWORD value)
{
	wrapV = value;
}

void Texture::SetFilterMAG(DWORD value)
{
	filterMAG = value;
}

void Texture::SetFilterMIN(DWORD value)
{
	filterMIN = value;
}

void Texture::SetFilterMIP(DWORD value)
{
	filterMIP = value;
}

void Texture::SetBlend(bool value)
{
	blendEnabled = value;
}

void Texture::SetBlendAdditive()
{
	blendEnabled = true;
	additiveBlendEnabled = true;
	multiplyBlendEnabled = false;
	alphaBlendEnabled = false;
}

void Texture::SetBlendMultiply()
{
	blendEnabled = true;
	additiveBlendEnabled = false;
	multiplyBlendEnabled = true;
	alphaBlendEnabled = false;
}

void Texture::SetBlendAlpha()
{
	blendEnabled = true;
	additiveBlendEnabled = false;
	multiplyBlendEnabled = false;
	alphaBlendEnabled = true;
}


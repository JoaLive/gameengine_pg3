#ifndef TEXTURE__H
#define TEXTURE__H

#include "stdafx.h"

class Texture
{
private:
	LPDIRECT3DTEXTURE9 tex;

	D3DXMATRIX texPosMat;
	D3DXMATRIX texScaleMat;
	D3DXMATRIX texRotMat;

	DWORD wrapU;
	DWORD wrapV;

	DWORD filterMAG;
	DWORD filterMIN;
	DWORD filterMIP;

	bool additiveBlendEnabled;
	bool multiplyBlendEnabled;
	bool alphaBlendEnabled;
	bool blendEnabled;
public:
	Texture(LPDIRECT3DTEXTURE9 _tex);
	~Texture();
	void SetTextureSettings(LPDIRECT3DDEVICE9 dev);
	LPDIRECT3DTEXTURE9 GetTexture();

	void SetPosition(float _x, float _y);
	void SetPosition(D3DXVECTOR2 pos);
	void RotateTexture(float angle);
	void ScaleTexture(D3DXVECTOR3 sca);
	void ScaleTexture(float _x, float _y);

	void SetWrapModeU(DWORD value);
	void SetWrapModeV(DWORD value);
	void SetFilterMAG(DWORD value);
	void SetFilterMIN(DWORD value);
	void SetFilterMIP(DWORD value);

	void SetBlend(bool value);
	void SetBlendAdditive();
	void SetBlendMultiply();
	void SetBlendAlpha();
};

#endif // !TEXTURE__h
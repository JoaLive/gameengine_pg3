 #include "stdafx.h"
#include "TilemapRender.h"
#include "Material.h"
#include "Game.h"
#include "Camera.h"
#include "Light.h"
#include "LightManager.h"

TilemapRender::TilemapRender(vector<vector<vector<int>>> tiles, float _width, float _height)
	: tileMap(tiles), tileWidth(_width), tileHeight(_height)
{

	Vertex vertexes[4] =
	{
		{ 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f ,1.0f },
		{ 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f ,1.0f },
		{ 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f ,0.0f },
		{ 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f ,0.0f },
	};

	WORD indices[6] = { 0, 3, 2 , 0, 2, 1};



	Game::GetInstance()->GetDevice()->CreateVertexBuffer(4 * sizeof(Vertex),
		0,
		CUSTOMFVF,
		D3DPOOL_MANAGED,
		&vb,
		NULL);

	Game::GetInstance()->GetDevice()->CreateIndexBuffer(6 * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&ib,
		NULL);

	VOID* data;
	vb->Lock(0, 0, &data, 0);
	memcpy(data, vertexes, 4 * sizeof(Vertex));
	vb->Unlock();

	ib->Lock(0, 0, &data, 0);
	memcpy(data, indices, 6 * sizeof(WORD));
	ib->Unlock();
}

TilemapRender::~TilemapRender()
{
}


void TilemapRender::RenderComposite(D3DXMATRIX * model)
{
	LPDIRECT3DDEVICE9 dev = Game::GetInstance()->GetDevice();

	dev->SetFVF(CUSTOMFVF);
	dev->SetStreamSource(0, vb, 0, sizeof(Vertex));
	dev->SetIndices(ib);

	D3DXMATRIX matSca;
	D3DXMatrixScaling(&matSca, tileWidth, tileHeight, 1);


	for (int indiceMat = 0; indiceMat < tileMap.size(); indiceMat++)
	{
		vector<vector<int>>* mat = &tileMap[indiceMat];
		for (int indiceFila = 0; indiceFila < mat->size(); indiceFila++)
		{
			vector<int>* fila = &mat->at(indiceFila);

			for (int indiceColumna = 0; indiceColumna < fila->size(); indiceColumna++)
			{
				int celda = fila->at(indiceColumna);
				if (celda > -1)
				{
					textures[celda]->SetTextureSettings(Game::GetInstance()->GetDevice());
					LPDIRECT3DTEXTURE9 tex = textures[celda]->GetTexture();
					dev->SetTexture(0, tex);

					float posX = tileWidth * indiceColumna;
					float posY = tileHeight * indiceFila;
					D3DXMATRIX matTrans;
					D3DXMatrixTranslation(&matTrans, posX, -posY, 3);

					D3DXMATRIX mat =   matSca * matTrans * *model;
					dev->SetTransform(D3DTS_WORLD, &mat);

					if (material != NULL)
					{
						if (LightManager::GetInstance()->lights.size() > 0)
						{
							//D3DXMATRIX mvp = mat * *Game::GetInstance()->GetMainCam()->GetViewMat() * *Game::GetInstance()->GetMainCam()->GetProyMat();
							D3DXMATRIX rotX;
							D3DXMATRIX rotY;
							D3DXMATRIX rotZ;
							D3DXVECTOR3 rota = GetWorldRotation();
							D3DXMatrixRotationX(&rotX, D3DXToRadian(rota.x));
							D3DXMatrixRotationY(&rotY, D3DXToRadian(rota.y));
							D3DXMatrixRotationZ(&rotZ, D3DXToRadian(rota.z));
							D3DXMATRIX rotmatrx = rotX * rotY * rotZ;

							bool basePass = true;

							for (int i = 0; i < LightManager::GetInstance()->lights.size(); i++)
							{
								if (LightManager::GetInstance()->lights[i]->GetType() == DIRECT_LIGHT)
								{
									int pass;

									//material->GetEffect()->SetMatrix("mvpMat", &mvp);
									material->GetEffect()->SetMatrix("modelMat", model);
									material->GetEffect()->SetMatrix("rotMat", &rotmatrx);
									material->GetEffect()->SetVector("lightDir", &LightManager::GetInstance()->lights[i]->GetDirection());
									material->GetEffect()->SetVector("lightCol", &LightManager::GetInstance()->lights[i]->GetColor());
									//material->GetEffect()->SetVector("camPos", &Game::GetInstance()->GetMainCam()->GetPosition());
									material->SetEffectsValues();
									if (basePass)
									{
										material->GetEffect()->SetVector("ambientCol", &material->GetAmbientColor());
										pass = 0;
									}
									else
										pass = 3;

									UINT passes;
									material->GetEffect()->Begin(&passes, 0);
									material->GetEffect()->BeginPass(pass);

									//dev->SetFVF(CUSTOMFVF);
									//dev->SetStreamSource(0, material->GetModel()->GetVertexBuffer(), 0, sizeof(Vertex));
									//dev->SetIndices(material->GetModel()->GetIndexBuffer());
									dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 6);

									material->GetEffect()->EndPass();
									basePass = false;
								}

								else if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT || LightManager::GetInstance()->lights[i]->GetType() == POINT_LIGHT)
								{
									int pass;

									D3DXVECTOR4 lightPos = { LightManager::GetInstance()->lights[i]->GetPosition().x, LightManager::GetInstance()->lights[i]->GetPosition().y, LightManager::GetInstance()->lights[i]->GetPosition().z, 0 };

									//material->GetEffect()->SetMatrix("mvpMat", &mvp);
									material->GetEffect()->SetMatrix("modelMat", model);
									material->GetEffect()->SetMatrix("rotMat", &rotmatrx);
									material->GetEffect()->SetVector("lightDir", &LightManager::GetInstance()->lights[i]->GetDirection());
									material->GetEffect()->SetVector("lightCol", &LightManager::GetInstance()->lights[i]->GetColor());
									material->GetEffect()->SetVector("lightPos", &lightPos);
									//material->GetEffect()->SetVector("camPos", &Game::GetInstance()->GetMainCam()->GetPosition());
									material->GetEffect()->SetFloat("lightRange", LightManager::GetInstance()->lights[i]->GetRange());

									material->SetEffectsValues();

									if (basePass)
									{
										material->GetEffect()->SetVector("ambientCol", &material->GetAmbientColor());
										if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT)
											pass = 1;
										else
											pass = 2;
									}
									else
									{
										if (LightManager::GetInstance()->lights[i]->GetType() == SPOT_LIGHT)
											pass = 4;
										else
											pass = 5;
									}

									UINT passes;
									material->GetEffect()->Begin(&passes, 0);
									material->GetEffect()->BeginPass(pass);
									//dev->SetFVF(CUSTOMFVF);
									//dev->SetStreamSource(0, material->GetModel()->GetVertexBuffer(), 0, sizeof(Vertex));
									//dev->SetIndices(material->GetModel()->GetIndexBuffer());
									dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 6);
									material->GetEffect()->EndPass();
									basePass = false;
								}
							}
							material->GetEffect()->End();
						}
						else
						{
							material->SetEffectsValues();

							UINT passes;
							material->GetEffect()->Begin(&passes, 0);
							for (int i = 0; i < passes; i++)
							{
								material->GetEffect()->BeginPass(i);
								//dev->SetFVF(CUSTOMFVF);
								//dev->SetStreamSource(0, material->GetModel()->GetVertexBuffer(), 0, sizeof(Vertex));
								//dev->SetIndices(material->GetModel()->GetIndexBuffer());
								dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 6);
								material->GetEffect()->EndPass();
							}
							material->GetEffect()->End();
						}
					}
					else
						dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 6);
				}
			}
		}
	}
}

void TilemapRender::AddTexture(Texture* tex)
{
	textures.push_back(tex);
}

void TilemapRender::SetMaterial(Material * mat)
{
	material = mat;
}

#ifndef TILEMAPRENDER__H
#define TILEMAPRENDER__H

#include "Composite.h"

class Material;
class Texture;
class TilemapRender : public Composite
{
private:
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
	vector<vector<vector<int>>> tileMap;
	vector<Texture*> textures;
	float tileWidth;
	float tileHeight;
	Material* material;

public:
	TilemapRender(vector<vector<vector<int>>> tiles, float _width, float _height);
	~TilemapRender();
	void RenderComposite(D3DXMATRIX* model) override;
	void AddTexture(Texture* tex);
	void SetMaterial(Material* mat);
};

#endif // !TILEMAPRENDER__H
